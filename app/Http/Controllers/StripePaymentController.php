<?php

   

namespace App\Http\Controllers;

   

use Illuminate\Http\Request;

use Session;

use Stripe;

   

class StripePaymentController extends Controller

{

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function stripe()

    {
        $json = [];
        $e = '';
        return view('stripe')->with(['json'=>$json,'e'=> $e]);

    }

  

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function stripePost(Request $request)

    {
        //dd($request->amount);        

        try{

            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $charge = Stripe\Charge::create ([

                "amount" => 100 * $request->amount,

                "customer" => $request->name,

                "currency" => "usd",

                "source" => $request->stripeToken,

                "description" => $request->description,

            ]);
            // $charge = Stripe\Charge::retrieve("ch_1ExTqDLl5Hg4PqmfTcmXL3KS");

             if($charge){
                /*return response()->json([

                'id' => $charge['id'],

                'transaction_id' =>  $charge['balance_transaction'],

                'amount'=> $charge['amount'],

                'status' => $charge['status'],

                'network_status' => $charge->outcome->network_status,

            ]); */
              /*\Stripe\Charge::update(
                'ch_1ExTqDLl5Hg4PqmfTcmXL3KS',
                [
                  'shipping' => ['name'=>'naveen','address'=>['line1' => '6735']],
                ]
              );
              dd($charge->shipping);*/


                $json = [

                'id' => $charge['id'],

                'transaction_id' =>  $charge['balance_transaction'],

                'amount'=> $charge['amount'],

                'status' => $charge['status'],

                'network_status' => $charge->outcome->network_status,

            ]   ;

                return view('stripe2')->with(['json'=>$json,'e'=>'']);
            }

        }

        catch (\Stripe\Error\Base $e) {
          // Code to do something with the $e exception object when an error occurs
          return view('stripe2')->with(['json'=>'','e'=>$e]);
          // echo($e->getMessage());
        } catch (Exception $e) {
          // Catch any other non-Stripe exceptions
        }



        
        //dd($charge['id']);

        


    }

}