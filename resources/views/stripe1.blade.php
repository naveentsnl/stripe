
<!DOCTYPE html>

<html>

<head>

	<title>Laravel 5 - Stripe Payment Gateway Integration Example</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	
	<form action="{{ route('stripe.post') }}" method="post">

		<label>Name on the card</label>
		<br>
		<input type="text" name="name">

		<br><br>
		
		<label>Card Number</label>
		<br>
		<input type="text" name="number">

		<br><br>

		<label>CVV</label>
		<br>
		<input type="text" name="cvv">

		<br><br>

		<label>Expiry Month</label>
		<br>
		<input type="text" name="month">

		<br><br>

		<label>Expiry Year</label>
		<br>
		<input type="text" name="year">

		<br><br>

		<label>Amount</label>
		<br>
		<input type="text" name="amount">

		<br><br>

		<input type="submit" name="submit">

	</form>

</body>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

